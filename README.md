# Muschain

Muschain is a minimal tracker-like sequencer for collaborative music composition, written in Python + QT.

## Some background information

This project was developed for my graduate thesis in University, so it's not expected to be fully functional nor free from bugs. Things you can do with it:

1) Create a new song: Choose a name for your Song. Use up to 4 different instruments (simple waveform synths) and up to 128 steps each track. You can choose from Pulse, Square, Sawtooth and Triangle.  
You can then select the BPM or "playback speed", wich can be changed afterwards before saving. This will create a Section for the new Song.  
**NOTE: ONCE THE SECTION IS SAVED, YOU CAN'T MODIFY IT ANYMORE.**  
This will upload the Song to the server, for other users to continue it.

2) Continue an existing song: Songs are made up from 4 Sections. When selecting this option, a random song from the server will be loaded for you to continue it.  
In the left part of the window you will see a secondary player with the previous section. The Song may have more sections before. You cannot change instruments, number of steps nor BPM. This will create a new Section for the loaded Song. The notes you place in it will be played right after de previous section.  
Again, once the Section is saved, it will be uploaded to the server and you won't be able to modify it.

3) Listen to completed songs: With this option you can listen to Songs that have all 4 Sections. The names of the users that composed each Section will be displayed alonside the Song's name.

It's been several years since I graduated and I no longer worked on this project for a while, so I thought I could publish the code for anyone to inspect it or modify it, if they wanted to.


## Setup

### Create an account on Back4App

To start using this software, first you have to create an account on [Back4App](https://back4app.com), an online database service used to store the data remotely. Back4App uses [Parse](https://parseplatform.org/), so you could technicaly use other service that implements Parse, or even run a Parse server instance locally, and it should also work with little or no modifications.

After registering the account, create a new app in the *Dashboard*. Take note of ***Application ID*** and ***REST API key*** under *App Settings > Security & Keys* menu.

### Copy configuration file

Copy `muschain.conf.example` file, rename it to `muschain.conf` and fill the values for the api endpoint, app key and api key.  
Set `debug=True` if you're just testing things and don't want to waste requests to the server.

### Setup a virtual environment

Just create a new environment for the project, using the following commands:  
> python -m venv .venv  
> source .venv/bin/activate

Then install the requirements from `requirements.txt` with:
> pip install -r requirements.txt

### Runing the app

Once all the requirements have been installed, just run the following command to get the app show its welcome screen:
> python muschain/App.py