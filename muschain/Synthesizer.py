from enum import Enum

import numpy as np
from scipy import signal


class Synthesizer(object):
    class WaveForm(Enum):
        PULSE = 'Pulse'
        SQUARE = 'Square'
        TRIANGLE = 'Triangle'
        SAWTOOTH = 'Sawtooth'

    def __init__(self, waveform):
        self.waveform = waveform

    def generateWave(self, frequency, rate, duration):
        size = round(rate*duration)
        if frequency:
            phase = 2 * np.pi * np.arange(size)*frequency/rate
            if self.waveform == Synthesizer.WaveForm.PULSE.value:
                return np.sin(phase)
            elif self.waveform == Synthesizer.WaveForm.SQUARE.value:
                return signal.square(phase)*0.5  # Volume too loud
            elif self.waveform == Synthesizer.WaveForm.TRIANGLE.value:
                return signal.sawtooth(phase, width=0.5)
            elif self.waveform == Synthesizer.WaveForm.SAWTOOTH.value:
                return signal.sawtooth(phase)*0.75  # Volume too loud
            else:
                raise TypeError("Unknown waveform: {}".format(self.waveform))
        else:
            return np.zeros(int(size))
