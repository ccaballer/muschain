import simpleaudio
import numpy as np
from math import pow
from Synthesizer import Synthesizer


NOTES = ['A', 'Bb', 'B', 'C', 'Db', 'D', 'Eb', 'E', 'F', 'Gb', 'G', 'Ab']
EMPTY = "-"
NOTE_END = "OFF"


class Song(object):
    def __init__(self, bpm, composers, completed=False, instruments=None, name=None, objectId=None, sections=None, **kwargs):
        self.bpm = bpm
        self.completed = completed
        self.composers = composers
        self.instruments = instruments or [Synthesizer.WaveForm.PULSE.value]
        self.name = name
        self.objectId = objectId
        self.sections = []

        if sections:
            for section_data in sections:
                section_data['instruments'] = self.instruments
                self.sections.append(Section(**section_data))
        else:
            self.sections.append(Section(self.composers[0], self.instruments, **kwargs))

    @property
    def data(self):
        data = {
            'bpm': self.bpm,
            'completed': self.completed,
            'composers': self.composers,
            'name': self.name,
            'objectId': self.objectId,
            'sections': [section.data for section in self.sections],
            'instruments': self.instruments
        }
        return data

    def addSection(self, composer):
        self.composers.append(composer)
        steps = self.sections[0].steps
        self.sections.append(Section(composer, self.instruments, steps=steps))

    def toAudio(self, rate, tick):
        output = np.array([])
        for section in self.sections:
            output = np.concatenate((output, section.toAudio(rate, tick)), axis=None)
        return output


class Section(object):
    def __init__(self, author, instruments, steps=16, tracks=None, **kwargs):
        self.author = author
        self.tracks = []
        if tracks:
            for track_notes, instrument in zip(tracks, instruments):
                self.tracks.append(
                    Track(instrument=instrument, notes=track_notes)
                )
        else:
            notes = ['-'] * steps
            for instrument in instruments:
                self.tracks.append(
                    Track(instrument=instrument, notes=notes)
                )

    def updateTracks(self, data):
        for track_data, track in zip(data, self.tracks):
            track.update(track_data)

    def toAudio(self, rate, tick):
        output = self.tracks[0].toAudio(rate, tick)
        for track in self.tracks[1:]:
            output += track.toAudio(rate, tick)
        output /= len(self.tracks)
        return output

    @property
    def data(self):
        data = {
            'author': self.author,
            'tracks': [track.notes for track in self.tracks]
        }

        return data

    @property
    def steps(self):
        return len(self.tracks[0].notes)

    @property
    def total_tracks(self):
        return len(self.tracks)


class Track(object):
    def __init__(self, instrument, notes=None):
        self.instrument = Synthesizer(instrument)
        self.notes = notes
        self._audio_notes = self._getAudioNotes()

    def _getAudioNotes(self):
        notes = []

        i = 0
        j = i + 1
        while i < len(self.notes):
            duration = 1
            while j < len(self.notes) and self.notes[j] == EMPTY:
                duration += 1
                j += 1
            notes.append(Note(data=self.notes[i], duration=duration))
            i = j
            j += 1

        return notes

    def update(self, data):
        self.notes = data['notes']
        self._audio_notes = self._getAudioNotes()

    def toAudio(self, rate, tick):
        output = np.array([])
        for note in self._audio_notes:
            wave = self.instrument.generateWave(
                frequency=note.frequency, rate=rate, duration=note.duration * tick
            )
            output = np.append(output, wave)
        return output


class Note(object):
    def __init__(self, data, duration=1):
        self.frequency = self._getNoteFrequency(data)
        self.is_silent = not bool(self.frequency)
        self.duration = duration

    def _getKeyNumber(self, note, octave):
        base_key = NOTES.index(note) + 1  # Add 1 since Array starts at 0
        if base_key > 3:  # Octave '1' starts with C
            return base_key + 12 * (octave - 1)
        else:
            return base_key + 12 * octave

    def _getNoteFrequency(self, input_note):
        if input_note in (EMPTY, NOTE_END):
            return 0
        else:
            note, octave = input_note.split("-")
            key = self._getKeyNumber(note, int(octave))
            freq = pow(2, float(key - 49) / 12) * 440  # A4 is Key 49, octave has 12 keys
            return freq

    #@property
    #def data(self):
    #    data = {
    #        'duration': self.duration,
    #        'frequency': self.frequency
    #    }
    #    return data


class Player(object):
    def __init__(self, rate=44100, tpb=4, channels=1):
        self._engine = simpleaudio
        self._rate = rate
        self._tpb = tpb
        self.bpm_list = self._getBpmList()
        self._channels = channels
        self.bpm = self._getDefaultBpm()
        self._tick = self._getTickFromBpm()
        self.current_song = None
        self.output = None

    def _normalizeAudioData(self, output):
        depth = np.max(np.abs(output))
        # Guard for silent track
        if depth:
            # Ensure that highest value is in 16-bit range
            output *= (2 ** 15 - 1) / depth
        # Convert to 16-bit data
        return output.astype(np.int16)

    def _getTickFromBpm(self):
        return 60 / (self.bpm * self._tpb)

    def _getBpmList(self):
        bpm_list = []
        # Find BPM from 30 to 300 within 5 distance
        for i in range(30, 301, 5):
            # Check if 'tick' got decimal
            if not (60 * self._rate) % (i * self._tpb):
                bpm_list.append(i)
        return bpm_list

    def _updateBpm(self, value):
        self.bpm = value
        self.current_song.bpm = value
        self._tick = self._getTickFromBpm()

    def loadSong(self, song):
        self.current_song = song
        self._updateBpm(song.bpm)
    
    def resetBpm(self):
        default_bpm = self._getDefaultBpm()
        self._updateBpm(default_bpm)

    def _getDefaultBpm(self):
        return self.bpm_list[int((len(self.bpm_list) - 1) / 2)]

    def _play(self, data):
        self.output = self._normalizeAudioData(data)
        self._engine.play_buffer(self.output, self._channels, 2, self._rate)
        # Wait for playback to finish before exiting
        # play_obj.wait_done()

    def playCurrentSection(self):
        section = self.current_song.sections[-1]
        data = section.toAudio(self._rate, self._tick)
        self._play(data)

    def playLastSection(self):
        section = self.current_song.sections[-2]
        data = section.toAudio(self._rate, self._tick)
        self._play(data)

    def playSong(self):
        data = self.current_song.toAudio(self._rate, self._tick)
        self._play(data)

    def stop(self):
        self._engine.stop_all()

    def updateBpmFromIndex(self, pos):
        new_bpm = self.bpm_list[pos]
        self._updateBpm(new_bpm)
        return self.bpm

    def getBpmIndex(self):
        return self.bpm_list.index(self.bpm)

    def getMaxBpmIndex(self):
        return len(self.bpm_list) - 1
