import json
import requests


def _createPointer(to, object_id):
    return {
        '__type': 'Pointer',
        'className': to,
        'objectId': object_id
    }


class ApiService(object):

    def __init__(self, config=None):
        self.base_url = config['api_url']
        self.debug = json.loads(config['debug'].lower())
        self.headers = {
            'X-Parse-Application-Id': config['app_key'],
            'X-Parse-REST-API-Key': config['rest_api_key']
        }

    def addHeader(self, header):
        self.headers.update(header)

    def removeHeader(self, key):
        del self.headers[key]

    def get(self, route, **kwargs):
        extra_headers = kwargs.get('headers', {})
        extra_headers.update(self.headers)
        params = kwargs.get('params', {})
        response = requests.get(url=self.base_url+route, headers=extra_headers, params=params)
        return response.json()

    def post(self, route, data, **kwargs):
        extra_headers = kwargs.get('headers', {})
        extra_headers.update(self.headers)
        response = requests.post(url=self.base_url+route, json=data, headers=self.headers)
        return response.json()

    def put(self, route, data):
        response = requests.put(url=self.base_url+route, json=data, headers=self.headers)
        return response.json()


class UserService(object):

    def __init__(self, api_service):
        self._base_service = api_service
        self.url_suffix = '/users'
        self.user = None

        if self._base_service.debug:
            self.user = {
                'username': 'test',
                'objectId': 'test'
            }
            self.signIn = lambda username, password: self.user
            self.logout = lambda: 'test'
            self.currentUser = lambda: self.user
            self.getUser = lambda user_id: self.user
            self.signUp = lambda username, password: self.user
            self.updateUser = lambda user_id, data: self.user

    def signIn(self, username, password):
        try:
            params = {'username': username, 'password': password}
            response = self._base_service.get(
                route='/login', headers={'X-Parse-Revocable-Session': '1'},
                params=params
            )
            token = response['sessionToken']
            self._base_service.addHeader({'X-Parse-Session-Token': token})
            self.user = response
            return response
        except KeyError:
            return response

    def logout(self):
        self._base_service.removeHeader('X-Parse-Session-Token')
        return 'Logged out successfully'

    def currentUser(self):
        return self._base_service.get(route=self.url_suffix + '/me')

    def getUser(self, user_id):
        url = self.url_suffix + '/' + user_id
        return self._base_service.get(route=url)

    def signUp(self, username, password):
        try:
            data = {'username': username, 'password': password}
            response = self._base_service.post(
                route=self.url_suffix, data=data, headers={'X-Parse-Revocable-Session': '1'}
            )
            token = response['sessionToken']
            self._base_service.addHeader({'X-Parse-Session-Token': token})
            self.user = {
                'objectId': response['objectId'],
                'username': username
            }
            return self.user

        except KeyError:
            return response

    def updateUser(self, user_id, data):
        url = self.url_suffix + '/' + user_id
        user = self.getUser(user_id)
        user.update(data)
        return self._base_service.put(route=url, data=user)


class SongService(object):

    def __init__(self, api_service):
        self.base_service = api_service
        self.url_suffix = '/classes/Song'

        if self.base_service.debug:
            self.current_song = {
                'bpm': 60,
                'composers' : ['test', 'test2'],
                "instruments": ['Pulse', 'Sawtooth', 'Pulse', 'Sawtooth'],
                "name": None,
                'objectId': None,
                "sections": [
                    {
                        'author': 'test',
                        "tracks": [
                            [
                                "C-4", "C-4", "Eb-4", "Eb-4", "Bb-3", "Bb-3", "D-4", "D-4", "OFF", "-",
                                "C-4", "B-2", "C-4", "B-1", "E-3", "Bb-1"
                            ],
                            [
                                "C-4", "C-4", "Eb-4", "Eb-4", "Bb-3", "Bb-3", "D-4", "D-4", "OFF", "-",
                                "C-4", "B-2", "C-4", "B-1", "E-3", "Bb-1"
                            ],
                            [
                                "C-4", "C-4", "Eb-4", "Eb-4", "Bb-3", "Bb-3", "D-4", "D-4", "OFF", "-",
                                "C-4", "B-2", "C-4", "B-1", "E-3", "Bb-1"
                            ],
                            [
                                "C-4", "C-4", "Eb-4", "Eb-4", "Bb-3", "Bb-3", "D-4", "D-4", "OFF", "-",
                                "C-4", "B-2", "C-4", "B-1", "E-3", "Bb-1"
                            ]
                        ]
                    },
                ]
            }
            self.current_section = {}
            self.createSong = lambda song_data: self.current_song
            self.getCompletedSongs = lambda: {'results': [self.current_song for i in range(4)]}
            self.getUnlockedSongs = lambda : {'results': [self.current_song]}
            self.lockSong = lambda song_data, user: self.current_song
            self.unlockSong = lambda song_data: self.current_song
            self.updateSong = lambda song_data: self.current_song

    def createSong(self, song_data):
        song_data['locked_by'] = None
        response = self.base_service.post(route=self.url_suffix, data=song_data)
        if not response.get('error'):
            song_data['objectId'] = response['objectId']
            return song_data
        else:
            return response

    def getCompletedSongs(self):
        params = {
            'where': json.dumps({
                'completed': True
            })
        }
        return self.base_service.get(route=self.url_suffix, params=params)

    def getUnlockedSongs(self):
        params = {
            'where': json.dumps({
                'locked_by': None,
                'completed': False
            })
        }
        return self.base_service.get(route=self.url_suffix, params=params)

    def lockSong(self, song_data, user):
        song_data['locked_by'] = _createPointer('_User', user['objectId'])
        return self.updateSong(song_data)

    def unlockSong(self, song_data):
        data = {
            'locked_by': None,
            'objectId': song_data['objectId']
        }
        return self.updateSong(data)

    def updateSong(self, song_data):
        song_data.setdefault('locked_by', None)
        url = self.url_suffix + '/{}'.format(song_data['objectId'])
        response = self.base_service.put(route=url, data=song_data)
        if not response.get('error'):
            return song_data
        else:
            return response
