import sys
from configparser import ConfigParser
from pathlib import Path
from PyQt5.QtWidgets import QApplication, QDialog
from backend.services import ApiService, UserService
from ui.windows import MainWindow
from ui.dialogs import Login


def start():
    app = QApplication(sys.argv)
    cfg_parser = ConfigParser()
    config_file = Path('muschain.conf').open()
    cfg_parser.read_file(config_file)
    config = cfg_parser['back4app']
    services = {'api': ApiService(config=config)}
    services['user'] = UserService(services['api'])
    login = Login(services)
    if login.exec_() == QDialog.Accepted:
        window = MainWindow(app, login.services)
        window.show()
    else:
        sys.exit()

    sys.exit(app.exec_())


if __name__ == '__main__':
    start()
