import json
from datetime import date
from pathlib import Path
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from backend.services import SongService
from ui.dialogs import WelcomeWizard, ClosePrompt
from ui.utils import QNoteBox
from Player import Player, Song, EMPTY, NOTES, NOTE_END

WINDOW_WIDTH = 400
WINDOW_HEIGHT = 600


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, app, services):
        super().__init__()
        self.app = app
        self.song_saved = False
        self.services = services
        self.services['song'] = SongService(self.services['api'])
        player = Player()
        self.statusBar().showMessage("Logged in as '{}'".format(self.services['user'].user['username']))

        # Setup menu bar
        self._initMenuBar()

        # Setup views
        self.tracker = TrackerView(self, player)
        self.setCentralWidget(self.tracker)

        self.resize(WINDOW_WIDTH, WINDOW_HEIGHT)
        self.centerWindow()
        self.setWindowTitle("Muschain - Untitled [1]")
        self.show()

        # Open Wizard dialog
        wizard = WelcomeWizard(self)
        if wizard.exec_() == QtWidgets.QDialog.Accepted:
            if wizard.data.get('continue_song'):
                self._continueSong(wizard.data['continue_song'])
            else:
                self._initNewSong(wizard.data)
        else:
            self.services['song'].unlockSong(wizard.data['continue_song'])

    def _continueSong(self, song_data):
        song = Song(**song_data)
        composer = self.services['user'].user['username']
        song.addSection(composer)
        self.tracker.player.loadSong(song)
        current_section = song.sections[-1]
        self.tracker.editor.initTable()
        self.tracker.editor.tracks = current_section.tracks
        self.tracker.prev_sec_zone.refresh_viewer()
        self.tracker.refreshBpmControl()
        self.tracker.control.bpm_slider.setDisabled(True)
        self.tracker.control.reset_bpm_btn.setDisabled(True)
        part = song.sections.index(current_section) + 1
        self.resize(WINDOW_WIDTH + 250*(current_section.total_tracks-1), WINDOW_HEIGHT)
        self.centerWindow()
        self.setWindowTitle("Muschain - {} [{}]".format(song.name, part))
        self.statusBar().showMessage("Song loaded successfully! Ready to continue")

    def _initLastSession(self, song_data):
        song = Song(**song_data)
        self.tracker.player.loadSong(song)
        self.tracker.refreshBpmControl()
        current_section = song.sections[-1]
        self.tracker.editor.tracks = current_section.tracks
        part = song.sections.index(current_section)
        self.centerWindow()
        self.setWindowTitle("Muschain - {} [{}]".format(song.name, part))
        self.statusBar().showMessage("Last session restored successfully!")

    def _initMenuBar(self):
        action_file = self.menuBar().addMenu("File")

        # New Action
        new_action = QtWidgets.QAction('New', self)
        new_action.setShortcut('Ctrl+N')
        new_action.setStatusTip('Create new section[Song]')
        new_action.triggered.connect(self.new)
        action_file.addAction(new_action)

        # Save Action
        self.save_action = QtWidgets.QAction('Save', self)
        self.save_action.setShortcut('Ctrl+S')
        self.save_action.setStatusTip('Save current section[Song]')
        self.save_action.triggered.connect(self.save)
        action_file.addAction(self.save_action)

        action_file.addSeparator()
        action_file.addAction("Quit")

    def _initNewSong(self, wizard_data):
        current_user = self.services['user'].user
        song_data = {
            'bpm': wizard_data['bpm'],
            'composers': [current_user['username']],
            'name': wizard_data['name'],
            'steps': wizard_data['steps'],
            'instruments': wizard_data['instruments']
        }
        song = Song(**song_data)
        self.tracker.player.loadSong(song)
        self.tracker.editor.initTable()
        width = int(WINDOW_WIDTH + WINDOW_WIDTH*(len(song.instruments)-1)*0.75)
        self.resize(width, WINDOW_HEIGHT)
        self.centerWindow()
        self.statusBar().showMessage("New Song created successfully!")
        self.setWindowTitle("Muschain - {} [1]".format(song.name))

    def new(self):
        if not self.song_saved:
            prompt = ClosePrompt()
            prompt.setText("You are about to start a new project. Any unsaved changes will be lost!")
            reply = prompt.exec_()
            if reply == QtWidgets.QMessageBox.Save:
                self.save()
            elif reply == QtWidgets.QMessageBox.Discard:
                if self.tracker.player.current_song.objectId:
                    self.services['song'].unlockSong(self.tracker.player.current_song.data)
            else:
                return

        self.setWindowTitle("Muschain - Untitled [1]")
        self._enableEditing()
        self.tracker.resetBpm()
        current_user = self.services['user'].user
        song_data = {
            'bpm': self.tracker.player.bpm,
            'composers': [current_user['username']],
        }
        song = Song(**song_data)
        self.tracker.player.loadSong(song)
        self.tracker.editor.initTable()
        self.tracker.prev_sec_zone.refresh_viewer()
        self.resize(WINDOW_WIDTH, WINDOW_HEIGHT)
        self.centerWindow()
        self.statusBar().showMessage('New project started successfully!')
        self.song_saved = False
        self.save_action.setEnabled(True)

        # Open Wizard dialog
        wizard = WelcomeWizard(self)
        if wizard.exec_() == QtWidgets.QDialog.Accepted:
            if wizard.data.get('continue_song'):
                self._continueSong(wizard.data['continue_song'])
            else:
                self._initNewSong(wizard.data)
        else:
            self.services['song'].unlockSong(wizard.data['continue_song'])

    def save(self, close=False):
        current_song = self.tracker.player.current_song
        current_section = current_song.sections[-1]
        current_section.updateTracks(self.tracker.editor.tracks)
        part = current_song.sections.index(current_section)+1

        # Open Dialog to ask for a Song Name if closed Wizard
        if not current_song.name:
            name, ok = QtWidgets.QInputDialog.getText(None, "Save new Song", "Enter your Song's name: ")
            if name and ok:
                current_song.name = name
                self.setWindowTitle("Muschain - {} [{}]*".format(current_song.name, part))
            else:
                return

        # Ask for confirmation before committing Section
        msg = "You are about to save current Section and mark it as 'Completed'\n" \
              "Continue? (This action cannot be undone)"
        reply = QtWidgets.QMessageBox.question(
            self, 'Message', msg, QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No
        )
        if reply == QtWidgets.QMessageBox.No:
            return

        if current_song.objectId:
            if len(current_song.sections) > 3:
                current_song.completed = True
            response = self.services['song'].updateSong(current_song.data)
            if response.get('error'):
                self.statusBar().showMessage("An error has occurred: '{}'".format(response['error']))
            else:
                self.statusBar().showMessage(
                    "[{}] Section {} saved successfully!".format(current_song.name, part))
                self._disableEditing()

        else:
            response = self.services['song'].createSong(current_song.data)
            if response.get('error'):
                self.statusBar().showMessage("An error has occurred: '{}'".format(response['error']))
            else:
                current_song.objectId = response['objectId']
                self.statusBar().showMessage("Song '{}' created successfully!".format(current_song.name))
                self._disableEditing()

        self.song_saved = True
        self.save_action.setDisabled(True)
        if close:
            self.app.quit()

    def centerWindow(self):
        frame_gm = self.frameGeometry()
        screen = QtWidgets.QApplication.desktop().screenNumber(QtWidgets.QApplication.desktop().cursor().pos())
        center_point = QtWidgets.QApplication.desktop().screenGeometry(screen).center()
        frame_gm.moveCenter(center_point)
        self.move(frame_gm.topLeft())

    def closeEvent(self, event):
        if self.song_saved:
            event.accept()
        else:
            prompt = ClosePrompt()
            reply = prompt.exec_()
            if reply == QtWidgets.QMessageBox.Save:
                event.ignore()
                self.save(close=True)
            elif reply == QtWidgets.QMessageBox.Discard:
                self.services['song'].unlockSong(self.tracker.player.current_song.data)
                event.accept()
            else:
                event.ignore()

    def _disableEditing(self):
        self.tracker.control.setDisabled(True)
        self.tracker.editor.setDisabled(True)
        self.tracker.prev_sec_zone.setDisabled(True)

    def _enableEditing(self):
        self.tracker.control.setEnabled(True)
        self.tracker.control.reset_bpm_btn.setEnabled(True)
        self.tracker.control.bpm_slider.setEnabled(True)
        self.tracker.editor.setEnabled(True)
        self.tracker.prev_sec_zone.setEnabled(True)


class TrackerView(QtWidgets.QWidget):
    def __init__(self, parent, player):
        super().__init__(parent)
        # Load audio engine
        self.player = player
        if self.player.current_song is None:
            current_user = self.parent().services['user'].user
            song_data = {
                'bpm': self.player.bpm,
                'composers': [current_user['username']],
            }
            song = Song(**song_data)
            self.player.loadSong(song)

        layout = QtWidgets.QHBoxLayout()
        self.prev_sec_zone = PreviousSectionViewer(self)
        self.prev_sec_zone.setSizePolicy(QtWidgets.QSizePolicy(
            QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Expanding))
        layout.addWidget(self.prev_sec_zone)

        inner_layout = QtWidgets.QVBoxLayout()

        # Add BPM, Play/Pause control
        self.control = ControlComponent(self)
        inner_layout.addWidget(self.control)

        # Add editor area
        self.editor = EditorComponent(self)
        inner_layout.addWidget(self.editor)

        layout.addLayout(inner_layout)

        self.setLayout(layout)

    def updateBpm(self):
        pos = self.control.bpm_slider.value()
        new_bpm = self.player.updateBpmFromIndex(pos)
        self.control.bpm_label.setText(str(new_bpm))

    def resetBpm(self):
        self.player.resetBpm()
        self.refreshBpmControl()

    def play(self):
        current_section = self.player.current_song.sections[-1]
        current_section.updateTracks(self.editor.tracks)
        self.player.playCurrentSection()

    def play_prev(self):
        self.player.playLastSection()

    def stop(self):
        self.player.stop()

    def refreshBpmControl(self):
        self.control.bpm_label.setText(str(self.player.bpm))
        self.control.bpm_slider.setValue(self.player.getBpmIndex())


class PreviousSectionViewer(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        layout = QtWidgets.QVBoxLayout()

        # Play/Stop buttons
        row = QtWidgets.QHBoxLayout()
        self.play_prev_btn = QtWidgets.QPushButton('Play')
        self.play_prev_btn.clicked.connect(self.parent().play_prev)
        row.addWidget(self.play_prev_btn)
        self.stop_prev_btn = QtWidgets.QPushButton('Stop')
        self.stop_prev_btn.clicked.connect(self.parent().stop)
        row.addWidget(self.stop_prev_btn)
        layout.addLayout(row)

        # Section viewer
        self.prev_sec_viewer = QtWidgets.QTableWidget()
        self.refresh_viewer()
        layout.addWidget(self.prev_sec_viewer)

        self.setLayout(layout)

    def refresh_viewer(self):
        self.prev_sec_viewer.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        if len(self.parent().player.current_song.sections) > 1:
            section = self.parent().player.current_song.sections[-2]
        else:
            section = None

        self.play_prev_btn.setEnabled(bool(section))
        self.stop_prev_btn.setEnabled(bool(section))
        if section:
            self.setFixedWidth(150 + 70*(section.total_tracks-1))
            self.prev_sec_viewer.setColumnCount(section.total_tracks)
            self.prev_sec_viewer.setHorizontalHeaderLabels([track.instrument.waveform for track in section.tracks])
            header = self.prev_sec_viewer.horizontalHeader()
            header.setSectionResizeMode(QtWidgets.QHeaderView.Fixed)
            header.setMinimumSectionSize(70)
            self.prev_sec_viewer.setRowCount(section.steps)
            for col, track in enumerate(section.tracks):
                for row, note in enumerate(track.notes):
                    item = QtWidgets.QTableWidgetItem(note)
                    item.setTextAlignment(Qt.AlignHCenter)
                    self.prev_sec_viewer.setItem(row, col, item)
            self.prev_sec_viewer.setEnabled(True)
        else:
            self.prev_sec_viewer.setColumnCount(0)
            self.prev_sec_viewer.setRowCount(0)
            self.setFixedWidth(150)
            self.prev_sec_viewer.setDisabled(True)

        self.prev_sec_viewer.resizeRowsToContents()
        self.prev_sec_viewer.resizeColumnsToContents()


class ControlComponent(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        layout = QtWidgets.QVBoxLayout()

        # BPM control
        row = QtWidgets.QHBoxLayout()
        row.addWidget(QtWidgets.QLabel("BPM: "))
        self.bpm_label = QtWidgets.QLabel(str(self.parent().player.bpm))
        row.addWidget(self.bpm_label)
        self.reset_bpm_btn = QtWidgets.QPushButton("Reset")
        self.reset_bpm_btn.clicked.connect(self.parent().resetBpm)
        row.addWidget(self.reset_bpm_btn)
        row.setAlignment(Qt.AlignLeft)
        layout.addLayout(row)
        self.bpm_slider = QtWidgets.QSlider(Qt.Horizontal)
        self.bpm_slider.setMinimum(0)
        self.bpm_slider.setMaximum(self.parent().player.getMaxBpmIndex())
        self.bpm_slider.setValue(self.parent().player.getBpmIndex())
        #self.bpm_slider.setDisabled(self.parent().player.current_section.part != 1)
        self.bpm_slider.valueChanged.connect(self.parent().updateBpm)
        layout.addWidget(self.bpm_slider)

        # Play/Pause control
        row = QtWidgets.QHBoxLayout()
        self.play_btn = QtWidgets.QPushButton("Play")
        self.play_btn.clicked.connect(self.parent().play)
        row.addWidget(self.play_btn)
        self.stop_btn = QtWidgets.QPushButton("Stop")
        self.stop_btn.clicked.connect(self.parent().stop)
        row.addWidget(self.stop_btn)
        layout.addLayout(row)

        self.setLayout(layout)


class EditorComponent(QtWidgets.QTableWidget):
    input_notes = [EMPTY]
    input_notes += NOTES
    input_notes.append(NOTE_END)
    octaves = [str(octave) for octave in range(9)]

    def __init__(self, parent=None):
        super().__init__(parent)
        self.initTable()

    def initTable(self):
        #self.setEnabled(True)
        section = self.parent().player.current_song.sections[-1]

        self.setColumnCount(section.total_tracks)
        self.setHorizontalHeaderLabels([track.instrument.waveform for track in section.tracks])
        header = self.horizontalHeader()
        header.setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        header.setMinimumSectionSize(150)
        self.setRowCount(section.steps)
        for col in range(self.columnCount()):
            for row in range(self.rowCount()):
                note_container = QtWidgets.QWidget()
                container_layout = QtWidgets.QHBoxLayout()
                note_button = QNoteBox()
                note_button.addItems(self.input_notes)
                container_layout.addWidget(note_button)
                octave_button = QNoteBox()
                octave_button.addItems(self.octaves)
                container_layout.addWidget(octave_button)
                note_container.setLayout(container_layout)
                self.setCellWidget(row, col, note_container)

        self.resizeRowsToContents()
        self.resizeColumnsToContents()

    @property
    def tracks(self):
        tracks = []
        for col in range(self.columnCount()):
            notes = []
            for row in range(self.rowCount()):
                container = self.cellWidget(row, col).layout()
                note = container.itemAt(0).widget().currentText()
                if note in (EMPTY, NOTE_END):
                    notes.append(note)
                else:
                    note += '-{}'.format(container.itemAt(1).widget().currentText())
                    notes.append(note)
            track_data = {'notes': notes}
            tracks.append(track_data)
        return tracks

    @tracks.setter
    def tracks(self, tracks):
        self.setColumnCount(len(tracks))
        self.setRowCount(len(tracks[0].notes))
        self.setHorizontalHeaderLabels([track.instrument.waveform for track in tracks])
        for track, col in zip(tracks, range(self.columnCount())):
            for note_data, row in zip(track.notes, range(self.rowCount())):
                container = self.cellWidget(row, col).layout()
                note_button = container.itemAt(0).widget()
                octave_button = container.itemAt(1).widget()
                if note_data in (EMPTY, NOTE_END):
                    index = note_button.findText(note_data)
                    note_button.setCurrentIndex(index)
                    octave_button.setCurrentIndex(0)
                else:
                    note, octave = note_data.split('-')
                    index = note_button.findText(note)
                    note_button.setCurrentIndex(index)
                    index = octave_button.findText(octave)
                    octave_button.setCurrentIndex(index)
