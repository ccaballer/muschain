import json
from enum import IntEnum
from random import randrange
from PyQt5.QtWidgets import (
    QDialog, QFormLayout, QLabel, QLineEdit, QSpacerItem, QPushButton, QCheckBox, QWizard, QWizardPage, QRadioButton,
    QComboBox, QSpinBox, QHBoxLayout, QHeaderView, QMessageBox, QVBoxLayout
)
from ui.utils import QInstrumentsTable
from Player import Song
from Synthesizer import Synthesizer


class ClosePrompt(QMessageBox):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Message")
        self.setIcon(QMessageBox.Question)
        self.setText("You are about to exit the program. Any unsaved changes will be lost!")
        self.setInformativeText("Do you want to save the current Song's Section?")
        self.setStandardButtons(QMessageBox.Save | QMessageBox.Discard)
        self.addButton(QMessageBox.Cancel)


class Login(QDialog):
    def __init__(self, services):
        super().__init__()
        self.services = services

        layout = QFormLayout()
        label = QLabel('Username: ')
        self.username_field = QLineEdit()
        self.username_field.setPlaceholderText('Username')
        layout.addRow(label, self.username_field)
        label = QLabel('Password: ')
        self.password_field = QLineEdit()
        self.password_field.setPlaceholderText('Password')
        self.password_field.setEchoMode(QLineEdit.Password)
        layout.addRow(label, self.password_field)
        self.signup = QCheckBox('Register for new account')
        self.signup.toggled.connect(self._toggleSignUp)
        layout.addRow(self.signup)
        self.error_text = QLabel()
        self.error_text.setStyleSheet('color: red')
        layout.addRow(self.error_text)

        layout.addItem(QSpacerItem(40, 20))
        self.login_btn = QPushButton('Sign In')
        self.login_btn.clicked.connect(self.login)
        layout.addRow(self.login_btn)
        self.setLayout(layout)

        self.setWindowTitle('Login')

    def _toggleSignUp(self):
        if self.signup.isChecked():
            self.login_btn.setText('Sign Up')
        else:
            self.login_btn.setText('Sign In')

    def login(self):
        username = self.username_field.text()
        password = self.password_field.text()
        if self.signup.isChecked():
            response = self.services['user'].signUp(username, password)
        else:
            response = self.services['user'].signIn(username, password)
        error = response.get('error', False)
        if error:
            self.error_text.setText(error)
        else:
            self.accept()


class WelcomeWizard(QWizard):
    class PageIndex(IntEnum):
        INTRO = 0
        CREATE = 1
        CONTINUE = 2
        LISTEN = 3

    def __init__(self, parent=None):
        super().__init__(parent)
        self.data = {}

        self.setDefaultProperty('QComboBox', 'currentText', QComboBox.currentIndexChanged)
        self.setDefaultProperty('QInstrumentsTable', 'currentData', QInstrumentsTable.itemChanged)

        response = self.parent().services['song'].getCompletedSongs()
        if response.get('results'):
            songs = []
            results = response['results']
            for song_data in results:
                songs.append(Song(**song_data))
            self.setPage(self.PageIndex.LISTEN, ListenCompletedSongs(self.parent().tracker.player, songs))

        self.setPage(self.PageIndex.INTRO, FirstPage(self, self.parent().services))
        self.setPage(self.PageIndex.CREATE, NewSongPage(self.parent().tracker.player.bpm_list))

        self.setWindowTitle('Welcome Wizard')

    def accept(self):
        if self.field('new_song'):
            song_data = self.data.pop('continue_song', None)
            if song_data:
                self.parent().services['song'].unlockSong(song_data)
        self.data['name'] = self.field('name')
        self.data['bpm'] = int(self.field('bpm'))
        self.data['steps'] = int(self.field('steps'))
        self.data['instruments'] = self.field('instruments')
        super().accept()


class FirstPage(QWizardPage):
    def __init__(self, parent, services):
        super().__init__(parent)
        self.services = services
        self.setTitle('Welcome to Muschain')
        self.setSubTitle('Please select an option:')
        layout = QFormLayout()
        self.new_song_btn = QRadioButton('Create a new Song')
        self.new_song_btn.setChecked(True)
        layout.addRow(self.new_song_btn)
        self.registerField('new_song', self.new_song_btn)
        self.continue_song_btn = QRadioButton('Continue an existing Song')
        self.continue_song_btn.toggled.connect(self._toggleFinalPage)
        continue_song = self.getSongToContinue()
        self.continue_song_btn.setEnabled(bool(continue_song))
        self.parent().data['continue_song'] = continue_song
        layout.addRow(self.continue_song_btn)
        self.listen_songs_btn = QRadioButton("Listen to completed Songs")
        self.listen_songs_btn.toggled.connect(self._toggleFinalPage)
        completed_songs = self.parent().page(WelcomeWizard.PageIndex.LISTEN)
        self.listen_songs_btn.setEnabled(bool(completed_songs))
        layout.addRow(self.listen_songs_btn)
        self.setLayout(layout)

    def _toggleFinalPage(self):
        if self.continue_song_btn.isChecked():
            self.setFinalPage(True)
        else:
            self.setFinalPage(False)

    def nextId(self):
        if self.new_song_btn.isChecked():
            return WelcomeWizard.PageIndex.CREATE
        if self.listen_songs_btn.isChecked():
            return WelcomeWizard.PageIndex.LISTEN
        else:
            return -1

    def getSongToContinue(self):
        available_songs = self.services['song'].getUnlockedSongs()
        selected_song = None
        if not available_songs.get('error'):
            choices = available_songs['results']
            if choices:
                selected_song = choices[randrange(len(choices))]
                selected_song.pop('createdAt', None)
                selected_song.pop('updatedAt', None)
                self.services['song'].lockSong(selected_song, self.services['user'].user)
        return selected_song


class NewSongPage(QWizardPage):
    def __init__(self, bpm_list, parent=None):
        super().__init__(parent)

        self.setTitle('Create a new Song')
        self.setSubTitle('Fill out the form to configure your Song')
        self.instruments = [waveform.value for waveform in Synthesizer.WaveForm]
        steps = [str(2**i) for i in range(4, 8)]
        layout = QHBoxLayout()

        # Song Form
        form = QFormLayout()
        self.name_field = QLineEdit()
        self.name_field.setPlaceholderText('My NewSong')
        form.addRow('Name: ', self.name_field)
        self.registerField('name*', self.name_field)

        label = QLabel('BPM: ')
        label.setToolTip('You can change this afterwards')
        self.bpm_selector = QComboBox()
        self.bpm_selector.addItems(str(bpm) for bpm in bpm_list)
        self.bpm_selector.setCurrentIndex(int(len(bpm_list)/2 - 1))
        form.addRow(label, self.bpm_selector)
        self.registerField('bpm', self.bpm_selector)

        self.num_tracks = QSpinBox()
        self.num_tracks.setRange(1, 4)
        self.num_tracks.valueChanged.connect(self._refreshTable)
        self.registerField('total_tracks', self.num_tracks)

        self.num_steps = QComboBox()
        self.num_steps.addItems(steps)
        self.registerField('steps', self.num_steps)

        # Track List
        self.instruments_container = QInstrumentsTable()
        self.instruments_container.setColumnCount(1)
        header = self.instruments_container.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Stretch)
        header.hide()
        self.instruments_container.setRowCount(self.num_tracks.value())
        for row in range(self.instruments_container.rowCount()):
            inst = QComboBox()
            inst.addItems(self.instruments)
            inst.setCurrentIndex(row)
            self.instruments_container.setCellWidget(row, 0, inst)
        self.instruments_container.resizeRowsToContents()
        self.registerField('instruments', self.instruments_container, )

        form.addRow('Tracks: ', self.num_tracks)
        form.addRow('Steps: ', self.num_steps)
        layout.addLayout(form)
        layout.addWidget(self.instruments_container)

        self.setLayout(layout)

    def _refreshTable(self):
        self.instruments_container.setRowCount(self.num_tracks.value())
        for row in range(self.instruments_container.rowCount()):
            inst = QComboBox()
            inst.addItems(self.instruments)
            inst.setCurrentIndex(row)
            self.instruments_container.setCellWidget(row, 0, inst)
        self.instruments_container.resizeRowsToContents()

    def nextId(self):
        return -1


class ListenCompletedSongs(QWizardPage):
    def __init__(self, player, songs):
        super().__init__()
        self.setTitle('Listen to completed Songs')
        self.setSubTitle("Select a Song and press 'Play' to listen")
        self.player = player
        self.songs = songs
        layout = QHBoxLayout()

        self.song_chooser = QComboBox()
        for song in songs:
            entry = "{} {}".format(song.name, song.composers)
            self.song_chooser.addItem(entry)
        layout.addWidget(self.song_chooser)
        self.play_btn = QPushButton('Play')
        self.play_btn.clicked.connect(self.play_song)
        layout.addWidget(self.play_btn)
        self.stop_btn = QPushButton('Stop')
        self.stop_btn.clicked.connect(self.player.stop)
        layout.addWidget(self.stop_btn)

        self.setLayout(layout)

    def play_song(self):
        song = self.songs[self.song_chooser.currentIndex()]
        self.player.loadSong(song)
        self.player.playSong()
