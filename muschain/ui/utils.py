from PyQt5.QtCore import pyqtProperty, Qt
from PyQt5.QtWidgets import QComboBox, QTableWidget


class QInstrumentsTable(QTableWidget):
    @pyqtProperty(list)
    def currentData(self):
        data = []
        for col in range(self.columnCount()):
            for row in range(self.rowCount()):
                data.append(self.cellWidget(row, col).currentText())
        return data


class QNoteBox(QComboBox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setFocusPolicy(Qt.StrongFocus)

    def wheelEvent(self, event):
        if self.hasFocus():
            return super().wheelEvent(event)
        else:
            event.ignore()
